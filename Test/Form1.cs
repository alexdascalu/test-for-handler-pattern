﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Test
{
    /// <summary>
    /// The MainForm class. In the constructor we add all handlers we want
    /// </summary>
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            // create the handler;
            ClickHandler handler = new ClickHandler();

            // add listener to button
            this.button1.Click += handler.click;
        }
    }
}
