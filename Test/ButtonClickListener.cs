﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    /// <summary>
    /// Interface to constrain a "Listener" class to have teh click method
    /// </summary>
    interface ButtonClickListener
    {
        void click(Object sender, EventArgs args);
    }
}
