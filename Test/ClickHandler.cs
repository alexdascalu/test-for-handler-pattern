﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Test
{
    /// <summary>
    /// Class holding method/methods for handling a click event
    /// </summary>
    class ClickHandler : ButtonClickListener
    {

        // in the constructor I can send all parameters I need (The main form, a textBox, etc);
        public ClickHandler()
        {

        }

        // method requested by the implementation of the interface
        public void click(object sender, EventArgs args)
        {
            MessageBox.Show("Click treated by handler");
        }
    }
}
